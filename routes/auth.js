module.exports = function(app, auth, bcrypt,cryptr, jwt, emailValidator, config, userModel){
  app.get('/auth/register/username/:username', function(req, res){
    userModel.findOne({ username: req.params.username.toLowerCase() }, function(err, doc){
      if(err){
        res.status(500).send(err);
        return;
      }

      if(doc){
        res.send(true);
      } else {
        res.send(false);
      }
    })
  });

  app.post('/auth/register', function(req, res){
    var username = req.body.username.toLowerCase();
    var regex = new RegExp("^[a-zA-Z0-9]*$");

    if(!regex.test(username)){ // verify only letters and numbers
      res.send({success: false, code: 2,  msg: "The Username can only contain letters and numbers"})
      return;
    }

    if(username.length > 15 || username.length < 1){ // check if username meets length requirements
      if(username.length > 15){
        var msg = "the Username exceeds the maximum length requirements of 15 characters"
      } else {
        var msg = "the Username cannot be empty"
      }

      res.send({success: false, code: 2,  msg: msg})
      return;
    }

    if(!emailValidator.validate(req.body.email)){
      res.send({success: false, code: 3,  msg: "Please provide a valid email address"})
      return;
    }



    userModel.findOne({username: username}, function(err, doc){ // check if username exists
      if(err){
        res.status(500).send(err);
        return;
      }

      if(doc){ // username already exists
          res.send({success: false, code: 1,  msg: "The Username already exists"})
          return;
      }




      bcrypt.genSalt(10, function(err, salt) {

        if(err){
          res.status(500).send(err);
          return;
        }

        bcrypt.hash(req.body.password, salt, function(err, hash) {
          // store user data in db
            if(err){
              res.status(500).send(err);
              return;
            }

            user = new userModel({
              givenName:req.body.givenName,
              familyName:req.body.familyName,
              displayName: req.body.username,
              email: req.body.email,
              username: username,
              password: hash,
            });



            user.save(function(err, doc){
              if(err){
                res.status(500).json(err);
                return;
              }


              res.json({success: true, msg: "Registration successfull!"
              });
            });

        });
      });
    });
  });

  app.post('/auth/login', auth.checkLockStatus, function(req, res){

    var username = req.body.username.toLowerCase();
    userModel.findOne({ username: username }, function(err, user){
        if(err){
          res.status(500).send(err);
          return;
        }

        if(user){ // user found
          bcrypt.compare(req.body.password, user.password, function(err, isValid) {
            // res == true
            if(err){
              res.status(500).send(err);
              return;
            }

            if(isValid === true){ // log in user
              jwt.sign({ user: user._id }, config.jwt.privateKey, {
                algorithm: config.jwt.algorithm
              }, function(err, token){
                var encryptedToken = cryptr.encrypt(token);
                res.setHeader('x-access-token', encryptedToken);
                res.send({success: true, msg: 'ok'});
              });

              return;
            } else { // bad password
              res.send({success: false, msg: "Bad Username or Password"});
              return;
            }

          });
        } else { // user not found
          res.send({success: false, msg: "Bad Username or Password"});
          return;
        }
    })

  });

  app.get('auth/me', function(req, res){
    res.send('hello');
  });

};
