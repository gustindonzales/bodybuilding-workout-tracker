module.exports = function(app, cryptr, jwt, async, auth, config, workoutModel, exerciseModel, setModel, userModel, buildInExerciseModel){


  app.post('/workout', auth.checkToken, function(req, res){
    /*var token = cryptr.decrypt(req.body.accessToken);
    // invalid token - synchronous
    try {
      var token = jwt.verify(token, config.jwt.privateKey);
    } catch(err) {

      res.status(401).send('bad token');
      return;
    }*/


    var w = new workoutModel({
      user: req.accessToken.user,
      name: req.body.workout.name,
      startDate: req.body.workout.startDate,
      endDate: req.body.workout.endDate,
      calendarEventColor: req.body.workout.color,
      exercises : []
    });

    w.save(function(err, doc){
      if(err){
        res.status(500).send(err);
        return;
      }
      res.send(doc);
    });


  });

  app.get('/workout', auth.checkToken, function(req, res){
    /*console.log(req.query.token);

    var token = cryptr.decrypt(req.query.token);
    // invalid token - synchronous
    try {
      var token = jwt.verify(token, config.jwt.privateKey);
    } catch(err) {

      res.status(401).send('bad token');
      return;
    }*/

    workoutModel.find({user: req.accessToken.user}).exec(function(err, doc){
      if(err){
          res.status(500).send(err);
          return;
      }

      res.send(doc);
    });

    return;
    var workouts = [
      {id: '9dfv9dfv9hd9fhv', start: '2016-09-07 05:30:00', end: '2016-09-07 06:30:00', title: 'Full Body', color: '#ff0000'},
      {id: '9dfv9dfv99asud0', start: '2016-09-05 04:30:00', end: '2016-09-05 05:40:00', title: 'Full Body'},
      {id: '9dfv9dfdfgdf4v9', start: '2016-09-04 04:30:00', end: '2016-09-04 05:40:00', title: 'Full Body'},
    ];

    res.send(workouts);
  });

  app.get('/workout/:id', function(req, res){
    var id = req.params.id;

    workoutModel.findById(id).populate([{
      path: 'exercises',
      populate: {
        path: 'sets',
        model: 'Set'
      }
    }, {
      path: 'user'
    }]).exec(function(err, doc){
      if(err){
          res.status(500).send(err);
          return;
      }



      res.send(doc);
      /*exerciseModel.find({workout: doc._id}).populate('workout').exec(function(err, doc){
        if(err){
            res.status(500).send(err);
            return;
        }

        workout['exercises'] = doc;

        res.send(doc);
      });*/

    });
  });

  app.put('/workout/:id', auth.checkToken, function(req, res){
    var id = req.params.id;



    workoutModel.findOneAndUpdate({_id: id}, req.body.workout, {new: true}, function(err, workout){
      if(err){
        res.status(500).send(err);
        return;
      }


      async.each(req.body.exercises, function(exercise){


        async.each(exercise.sets, function(set){

          var s = {
            reps: set.reps,
            weight: set.weight,
            note: set.note
          };


          setModel.findOneAndUpdate({_id: set._id}, s, {new: true}, function(err, doc){
            if(err){
              //res.status(500).send(err);
              //console.log(err);
              return;
            }
            //console.log(doc);
          });
        });
        var e = {
          name: exercise.name,
          note: exercise.note
        };

        exerciseModel.findOneAndUpdate({_id: exercise.id, workout: id}, e, {new: true}, function(err, doc){
          if(err){
            res.status(500).send(err);
            return;
          }

        });
      });
      res.send(workout);
    });


  });

  app.delete('/workout/:id', auth.checkToken, function(req, res){
    var id = req.params.id;


    exerciseModel.find({workout: id}, function(err, doc){ //find all exercises in workout
      async.each(doc, function(exercise, callback) {
        exerciseModel.remove({workout: id}, function(err){ // delete exercises
          if(err){
            res.status(500).send(err);
            return;
          }

          setModel.remove({exercise: exercise._id}, function(err, doc){ // delete sets
            if(err){
              res.status(500).send(err);
              return;
            }


          });

        });
      });
    });





        workoutModel.findOneAndRemove({_id: id}, function(err, doc){ // delete workout
          if(err){
            res.status(500).send(err);
            return;
          }
          res.send(doc);
        });


  });


  app.get('/workout/:id/exercise/:eId', auth.checkToken, function(req, res){
    exerciseModel.findById(req.params.eId).populate('sets').exec(function(err, doc){
      if(err){
          res.status(500).send(err);
          return;
      }

      res.send(doc);
    });
  });

  app.put('/workout/:id/exercise/:eId', auth.checkToken, function(req, res){
    var id = req.params.id;
    var eId = req.params.eId;

    exerciseModel.findOneAndUpdate({_id: eId}, req.body.exercise, {new: true}, function(err, exercise){
      if(err){
        res.status(500).send(err);
        return;
      }

      res.send(exercise);
    });
  });

  app.post('/workout/:id/exercise', function(req, res){
    var id = req.params.id;
    var e = new exerciseModel({
      workout: id,
      name: "",
      note: "",
      sets: []
    });

    e.save(function(err, doc){
      if(err){
        res.status(500).send(err);
        return;
      }
      var exercise = doc;

      workoutModel.findOneAndUpdate({_id: id}, {$push: {exercises: doc._id}}, {new: true}).populate('exercises user').exec(function(err, doc){
        res.send(exercise);
      });
    });
  });


  app.delete('/workout/:id/exercise/:eId', auth.checkToken, function(req, res){
    var id = req.params.id;
    var eId = req.params.eId;


    exerciseModel.findOneAndRemove({_id: eId}, function(err, exercise){ //find all exercises in workout
      if(err){
        res.status(500).send(err);
        return;
      }

      setModel.remove({exercise: eId}, function(err, doc){ // delete sets
        if(err){
          res.status(500).send(err);
          return;
        }

        res.send(exercise);
      });
    });
  });


  app.post('/workout/:id/exercise/:eId/set', function(req, res){
    var id = req.params.id;
    var eId = req.params.eId;

    var s = new setModel({
      exercise: eId,
      reps: 0,
      weight: 0,
      note: ""
    });

    s.save(function(err, doc){
      if(err){
        res.status(500).send(err);
        return;
      }
      var set = doc;

      exerciseModel.findOneAndUpdate({_id: eId}, {$push: {sets: set._id}}, {new: true}).populate('sets').exec(function(err, doc){
        res.send(set);
      });
    });
  });

  app.put('/workout/:id/exercise/:eId/set/:sId', auth.checkToken, function(req, res){
    var id = req.params.id;
    var eId = req.params.eId;
    var sId = req.params.sId;

    setModel.findOneAndUpdate({_id: sId}, req.body.set, {new: true}, function(err, set){
      if(err){
        res.status(500).send(err);
        return;
      }

      res.send(set);
    });
  });

  app.delete('/workout/:id/exercise/:eId/set/:sId', auth.checkToken, function(req, res){ // delete given set
    var id = req.params.id;
    var eId = req.params.eId;
    var sId = req.params.sId;

    setModel.findOneAndRemove({_id: sId}, function(err, doc){ //find all exercises in workout

      if(err){
        res.status(500).send(err);
        return;
      }

      res.send(doc);
    });
  });


  /*app.post('/workoutRepository', function(req, res){
    var e = new buildInExerciseModel({
      name: req.body.exercise,
      category: req.body.category,
      level: req.body.level,
    });

    e.save(function(err) {
      if(err){
        res.status(500).send(err);
        return;
      }

      res.send('ok');
    });
  });*/

}
