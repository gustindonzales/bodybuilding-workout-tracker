module.exports = function(app, auth, bcrypt,cryptr, jwt, emailValidator, config, userModel){
  app.get('/auth/register/username/:username', function(req, res){
    userModel.findOne({ username: req.params.username.toLowerCase() }, function(err, doc){
      if(err){
        res.status(500).send(err);
        return;
      }

      if(doc){
        res.send(true);
      } else {
        res.send(false);
      }
    })
  });

  app.put('/user/password', auth.checkToken, function(req, res){

    var currentPassword = req.body.passwords.currentPassword;
    var newPassword = req.body.passwords.newPassword;
    var confirmNewPassword = req.body.passwords.confirmNewPassword;


    if(currentPassword === "" || typeof currentPassword === 'undefined'){
      res.send({success: false, code: 1,  msg: "The current password cannot be empty"});
      return;
    }

    if(newPassword === "" || typeof newPassword === 'undefined'){
      res.send({success: false, code: 2,  msg: "The new password cannot be empty"});
      return;
    }

    if(newPassword !== confirmNewPassword){
      res.send({success: false, code: 3,  msg: "The two Password do not match"});
      return;
    }


    userModel.findById(req.accessToken.user, function(err, user){
        if(err){
          res.status(500).send(err);
          return;
        }

        if(user){ // user found
          bcrypt.compare(req.body.passwords.currentPassword, user.password, function(err, isValid) {
            // res == true
            if(err){
              res.status(500).send(err);
              return;
            }

            if(isValid === true){ // passwords match

              bcrypt.genSalt(10, function(err, salt) {

                if(err){
                  res.status(500).send(err);
                  return;
                }

                bcrypt.hash(req.body.passwords.newPassword, salt, function(err, hash) {
                  // store user data in db
                    if(err){
                      res.status(500).send(err);
                      return;
                    }

                    userModel.findOneAndUpdate({_id: req.accessToken.user}, {password: hash}, {new: true}, function(err, doc){
                      if(err){
                        res.status(500).json(err);
                        return;
                      }


                      res.json({success: true, msg: "Password changed successfully"});
                      return;
                      });
                    });

                });

              return;
            } else { // bad password
              res.send({success: false, code: 4,  msg: "The current password is not correct"});
              return;
            }

          });
        } else { // user not found
          res.send({success: false, code: 5,  msg: "Couldn't locate user data. Please try again later"});
          return;
        }
    })

return;

  bcrypt.genSalt(10, function(err, salt) {

    if(err){
      res.status(500).send(err);
      return;
    }

    bcrypt.hash(newPassword, salt, function(err, hash) {
      // store user data in db
        if(err){
          res.status(500).send(err);
          return;
        }

        userModel.findOneAndUpdate({_id: req.accessToken.user}, {password: hash}, function(err){
          if(err){
            res.status(500).send(err);
            return;
          }

          res.json({success: true, msg: "Password changed successfully!"});
        });
      });
    });
  });

  app.post('/auth/login', auth.checkLockStatus, function(req, res){

    var username = req.body.username.toLowerCase();
    userModel.findOne({ username: username }, function(err, user){
        if(err){
          res.status(500).send(err);
          return;
        }

        if(user){ // user found
          bcrypt.compare(req.body.password, user.password, function(err, isValid) {
            // res == true
            if(err){
              res.status(500).send(err);
              return;
            }

            if(isValid === true){ // log in user
              jwt.sign({ user: user._id }, config.jwt.privateKey, {
                algorithm: config.jwt.algorithm
              }, function(err, token){
                var encryptedToken = cryptr.encrypt(token);
                res.setHeader('x-access-token', encryptedToken);
                res.send({success: true, msg: 'ok'});
              });

              return;
            } else { // bad password
              res.send({success: false, msg: "Bad Username or Password"});
              return;
            }

          });
        } else { // user not found
          res.send({success: false, msg: "Bad Username or Password"});
          return;
        }
    })

  });

  app.get('auth/me', function(req, res){
    res.send('hello');
  });

};
