module.exports = function(mongoose){
  var ObjectId = mongoose.Schema.ObjectId;
  var builtInExerciseSchema = mongoose.Schema({
      name     : String,
      category : String,
      level    : String
  });

  var BuiltInExercise = mongoose.model('BuiltInExercise', builtInExerciseSchema);

  return BuiltInExercise;
};
