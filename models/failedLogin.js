module.exports = function(mongoose){
  var ObjectId = mongoose.Schema.ObjectId;
  var failedLoginSchema = mongoose.Schema({
      remoteAddress : {type: String, default: ""},
      attempts      : {type: String, default: ""},
      lastAttempt   : Date,
      locked        : {type: Boolean, default: false},
      unlockTime    : Date
  });

  var FailedLogin = mongoose.model('FailedLogin', failedLoginSchema);

  return FailedLogin;
};
