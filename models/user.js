module.exports = function(mongoose){
  var userSchema = mongoose.Schema({
      givenName   : String,
      familyName  : String,
      displayName : String,
      email       : String,
      username    : String,
      password    : String,
  });

  var User = mongoose.model('User', userSchema);

  return User;
};
