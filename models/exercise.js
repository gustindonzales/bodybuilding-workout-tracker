module.exports = function(mongoose, workoutModel, setModel){
  var ObjectId = mongoose.Schema.ObjectId;
  var exerciseSchema = mongoose.Schema({
      workout : {type: ObjectId, ref: "WWorkout"},
      name    : String,
      note    : { type: String, default: "" },
      sets    : [{type: ObjectId, ref: "Set"}],
  });

  var Exercise = mongoose.model('Exercise', exerciseSchema);

  return Exercise;
};
