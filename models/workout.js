module.exports = function(mongoose, exerciseModel, userModel){
  var ObjectId = mongoose.Schema.ObjectId;
  var workoutSchema = mongoose.Schema({
      user               : { type: ObjectId, ref: "User" },
      name               : {type: String, default: ""},
      description        : {type: String, default: ""},
      startDate          : Date,
      endDate            : Date,
      calendarEventColor : { type: String, default: "#3a87ad"},
      exercises          : [{type: ObjectId, ref: "Exercise"}]
  });

  var Workout = mongoose.model('Workout', workoutSchema);

  return Workout;
};
