module.exports = function(mongoose, exerciseModel){
  var ObjectId = mongoose.Schema.ObjectId;
  var setSchema = mongoose.Schema({
      exercise : { type: ObjectId, ref: "Exercise" },
      reps     : {type: Number, default: 0},
      weight   : {type: Number, default: 0},
      note     : {type: String, default: ""}
  });

  var Sets = mongoose.model('Set', setSchema);

  return Sets;
};
