# Bodybuilding Workout Tracker

The Bodybuilding workout tracker offers a system to log and track each of your workouts.

### Compatibility

There is an issue on iphones where the Modal to create a new workout will not display after you click the calendar day. I've found a couple work arounds which will remedy this bug temporarily. the solutions I've found are listed below.  
- put your phone in landscape and the modal will display after pressing the calendar day.  
- swipe right or left on the day you wish to create the event for. this seems to trigger the function call.  
  
I've tested on Chrome desktop and Android. these platforms work fine.  
  
### Prerequisites

Make sure the following are installed.  
- [Node.js with NPM][1]  
- Bower `npm install -g bower`  
- [MongoDB][2]  

[1]: https://nodejs.org/en/download/ "Node.js"
[2]: https://www.mongodb.com/ "MongoDB"

### Installing

1. Clone the repo. `git clone https://gustindonzales@bitbucket.org/gustindonzales/bodybuilding-workout-tracker.git`
2. cd into the root of the project and run `npm install`
3. cd into ./public/sb-admin-angular and run `npm install`

### Starting the Server
1. Start MongoDB `mongod`
2. cd into the root of the project and run `node app`