/*eslint-env node*/
var express = require('express');
var bodyParser = require('body-parser');
var cfenv = require('cfenv');
var cors = require('cors');

var config         = require("./config.json");
var bcrypt         = require('bcryptjs');
var Cryptr         = require('cryptr');
var jwt            = require('jsonwebtoken');
var fs             = require('fs');
var http           = require('http');
var https          = require('https');
var emailValidator = require("email-validator");
var moment         = require('moment');
var async          = require('async');

var privateKey        = fs.readFileSync('server.key', 'utf8');
var certificate       = fs.readFileSync('server.crt', 'utf8');
var credentials       = {key: privateKey, cert: certificate};

// create a new express server
var app = express();



// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

var mongoose = require('mongoose');

cryptr = new Cryptr(config.encryption.privateKey);

// models
var failedLoginModel = require('./models/failedLogin')(mongoose);
var userModel = require('./models/user')(mongoose);
var workoutModel = require('./models/workout')(mongoose, exerciseModel, userModel);
var exerciseModel = require('./models/exercise')(mongoose, workoutModel, setModel);
var setModel = require('./models/set')(mongoose, exerciseModel);
var buildInExerciseModel = require('./models/builtInExercise')(mongoose);



//modules
var auth = require('./modules/auth')(jwt, cryptr, config, moment, failedLoginModel);

mongoose.connect('mongodb://localhost/bbwt');


var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

function ensureSecure(req, res, next){
  if(req.secure){
    // OK, continue
    return next();
  };
  res.redirect('https://'+req.hostname+':6003'+req.url); // handle port numbers if you need non defaults

};

app.all('*', ensureSecure); // at top of routing calls


// serve the files out of ./public as our main files
if(config.app.isProduction){
  app.use('/', express.static(__dirname + '/public/sb-admin-angular/dist'));
  app.use('/bower_components', express.static(__dirname + '/public/sb-admin-angular/bower_components'));
} else {
  app.use('/', express.static(__dirname + '/public/sb-admin-angular/app'));
  app.use('/bower_components', express.static(__dirname + '/public/sb-admin-angular/bower_components'));
}

// allow CORS
app.use(cors({exposedHeaders: 'x-access-token'}));


require('./routes/auth')(app, auth, bcrypt, cryptr, jwt, emailValidator, config, userModel);
require('./routes/user')(app, auth, bcrypt, cryptr, jwt, emailValidator, config, userModel);
require('./routes/log')(app, cryptr, jwt, async, auth, config, workoutModel, exerciseModel, setModel, userModel, buildInExerciseModel);

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();


httpServer.listen(6002, function(){
  console.log('http server listening on port 6002');
});
httpsServer.listen(6003, function(){
  console.log('https server listening on port 6003');
});

return;
// start server on the specified port and binding host
app.listen(appEnv.port, '0.0.0.0', function() {
  // print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});
