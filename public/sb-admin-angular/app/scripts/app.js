'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'angular-loading-bar',
    'ui.calendar'
  ])
  .config(['$stateProvider','$provide','$httpProvider','$urlRouterProvider','$ocLazyLoadProvider',function ($stateProvider,$provide,$httpProvider,$urlRouterProvider,$ocLazyLoadProvider) {

    // Intercept http calls.
    $provide.factory('ErrorHttpInterceptor', function ($q, $window, $location) {
        var errorCounter = 0;
        function notifyError(rejection){
          if(rejection.status === 401) return;
            $.bigBox({
                title: rejection.status + ' ' + rejection.statusText,
                content: rejection.data,
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                number: ++errorCounter,
                timeout: 6000
            });
        }
        function handleError(rejection){
          if(rejection.status === 401){
            $window.localStorage.removeItem('accessToken');
            $location.path('/login');
            return;
          }

          return;
            $.bigBox({
                title: rejection.status + ' ' + rejection.statusText,
                content: rejection.data,
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                number: ++errorCounter,
                timeout: 6000
            });
        }

        return {

            // On request failure
            requestError: function (rejection) {
                // show notification
                //notifyError(rejection);
                handleError(rejection);
                // Return the promise rejection.
                return $q.reject(rejection);
            },

            // On response failure
            responseError: function (rejection) {
                // show notification
                //notifyError(rejection);
                handleError(rejection);
                // Return the promise rejection.
                return $q.reject(rejection);
            }
        };
    });
    // Add the interceptor to the $httpProvider.
    $httpProvider.interceptors.push('ErrorHttpInterceptor');

    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

    $urlRouterProvider.otherwise('/dashboard/log');

    $stateProvider
      .state('dashboard', {
        url:'/dashboard',
        controller: 'MainCtrl',
        templateUrl: 'views/main.html',
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[
                    'scripts/directives/header/header.js',
                    'scripts/directives/header/header-notification/header-notification.js',
                    'scripts/directives/sidebar/sidebar.js',
                    'scripts/directives/sidebar/sidebar-search/sidebar-search.js',
                    'scripts/services/userFactory.js',
                    'scripts/services/authenticationFactory.js',
                    'scripts/controllers/main.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('dashboard.log',{
        url:'/log',
        controller: 'LogCtrl',
        templateUrl:'views/log.html',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'scripts/controllers/log.js',
              'scripts/services/authenticationFactory.js'
              ]
            })
          }
        }
      })
      .state('dashboard.progress',{
        templateUrl:'views/progress.html',
        url:'/progress'
    })
      .state('login',{
        controller: 'LoginCtrl',
        templateUrl:'views/login.html',
        url:'/login',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'scripts/services/userFactory.js',
              'scripts/services/flashFactory.js',
              'scripts/services/authenticationFactory.js',
              'scripts/controllers/login.js'
              ]
            })
          }
        }
    })
      .state('register',{
        templateUrl:'views/register.html',
        url:'/register',
        controller: 'RegisterCtrl',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'scripts/services/userFactory.js',
              'scripts/services/authenticationFactory.js',
              'scripts/controllers/register.js',
              'scripts/directives/register/compareTo.js'
              ]
            })
          }
        }
    })
      .state('dashboard.leaderboard',{
        templateUrl:'views/leaderboard.html',
        url:'/chart',
        controller:'ChartCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'bower_components/angular-chart.js/dist/angular-chart.min.js',
                'bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/chartContoller.js']
            })
          }
        }
    })
      .state('dashboard.user',{
        templateUrl:'views/account.html',
        url:'/user',
        controller: 'UserCtrl',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'scripts/controllers/user.js',
              'scripts/directives/register/compareTo.js'
              ]
            })
          }
        }
    })
  }])
  .run(function($rootScope, $window, $location, $state){
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
      var nonAuthenticationPages = ['login', 'register'];

      if(nonAuthenticationPages.indexOf(toState.name)  === -1){ // not on login page

        if($window.localStorage.getItem('accessToken') === null){ // access token not present
          event.preventDefault();
          $state.go('login');


        }
      } else { // on login page

        if($window.localStorage.getItem('accessToken') !== null){ // access token present
          event.preventDefault();
          $state.go('dashboard.log'); // go to login

        }
      }
    });


  });
