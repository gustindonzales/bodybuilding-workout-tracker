'use strict';
angular.module('sbAdminApp')
  .controller('RegisterCtrl', function($scope, $http, $location, $window, $timeout, $log, UserService, AuthenticationService) {
    $scope.user = {};

    $scope.register = function(valid){

      if(valid){
        $scope.dataLoading = true;
        UserService.Create($scope.user)
            .then(function (response) {
              $scope.message = response.msg;
              $scope.code = response.code;

              if($scope.code === 11000){ //duplicate email error
                $scope.message = response.msg = 'The email provided already belongs to an account';
              }

              $scope.hasMessage = true;
                if (response.success) { // successfull registration
                  $scope.success = true;
                  $scope.type = 'success';
                  var counter = 1000;
                  //$timeout(function(){
                    AuthenticationService.Login($scope.user.username, $scope.user.password, function(response){
                      if (response.success) {
                          $location.path('/dashboard/log');
                      } else {

                      }
                    });
                  //}, counter);

                } else { // unsuccessfull registration
                    $scope.type = 'error';
                    $scope.dataLoading = false;

                    var usernameCodes = [1,2];
                    var emailCodes = [3, 11000];
                    if(usernameCodes.indexOf($scope.code) > -1){ // bad username
                      var element = $window.document.getElementById("username");
                      element.focus();
                      $scope.user.username = '';
                    } else if(emailCodes.indexOf($scope.code) > -1){ // bad email
                      var element = $window.document.getElementById("email");
                      element.focus();
                      $scope.user.email = '';
                    }
                }
            });
      }
    };


});
