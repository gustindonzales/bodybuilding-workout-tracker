'use strict';
angular.module('sbAdminApp')
  .controller('UserCtrl', function($scope, $http, $location, $window, $timeout, $log) {
    var accessToken = $window.localStorage.getItem('accessToken');
    $scope.user = {};

    $scope.changePassword = function(valid){
      if(valid){
        $http({
          method: 'POST',
          url: '/user/password',
          data: { password: $scope.user.password, accessToken: accessToken }
        }).then(function(response){
          $scope.message = response.data.msg;
          $scope.hasMessage = true;
          $scope.type = 'success';
          $scope.user.password = {};

          $timeout(function(){
            $scope.hasMessage = false;
          }, 3000);
        });
      }
    }
});
