'use strict';
angular.module('sbAdminApp')
  .controller('LoginCtrl', function($scope, $http, $location, $window, $log, AuthenticationService, FlashService) {
    $scope.user = {};



    $scope.login = function(valid){

      if(valid){
            AuthenticationService.Login($scope.user.username, $scope.user.password, function (response) {

                if (response.success) {
                    $location.path('/dashboard/log');
                } else {
                  $scope.message = response.msg;
                  $scope.hasMessage = true;
                  $scope.type = 'error';
                  $scope.user.password = '';
                }
            });
      }
    };


});
