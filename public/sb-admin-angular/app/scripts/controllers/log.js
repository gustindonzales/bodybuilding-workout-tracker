'use strict';
angular.module('sbAdminApp')
    .controller('LogCtrl', function($scope, $rootScope, $uibModal, $log, $http, AuthenticationService) {
        $scope.eventSources = [
            []
        ];

        var token = AuthenticationService.GetToken();

        $rootScope.dataLoading = true;
        $http({
            method: 'GET',
            url: './workout?accessToken=' + token,
        }).then(function(response) {

            for (var i = 0; i < response.data.length; i++) {
                var e = {
                    id: response.data[i]._id,
                    start: moment(response.data[i].startDate),
                    end: moment(response.data[i].endDate),
                    title: response.data[i].name,
                    color: response.data[i].calendarEventColor,
                    stick: true
                };
                //response.data[i].stick = true; // fixes issue with events dissapearing



                $scope.eventSources[0].push(e);

            }
            $rootScope.dataLoading = false;
        }, function(err) {
            console.log(err);
            $rootScope.dataLoading = false;
        });



        /* config object */
        $scope.uiConfig = {
            calendar: {
                height: 450,
                editable: false,
                selectable: true,
                selectHelper: true,
                unselectAuto: true,
                longPressDelay: 300,

                header: {
                    left: 'month agendaWeek agendaDay',
                    center: 'title',
                    right: 'today prev,next'
                },
                select: function(start, end) {
                    $scope.start = start;
                    $scope.end = end;
                    $scope.openModal(false, 'md');
                },


                eventClick: function(e) {
                    $rootScope.dataLoading = true;
                    $http({
                        method: 'GET',
                        url: './workout/' + e.id
                    }).then(function(response) {

                        var workout = {
                            id: response.data._id,
                            start: moment(response.data.startDate),
                            end: moment(response.data.endDate),
                            title: response.data.name,
                            color: response.data.calendarEventColor,
                            exercises: []
                        };


                        $scope.selectedWorkout = workout;
                        $scope.openModal(true, 'md');
                        $rootScope.dataLoading = false;
                    }, function(err) {
                        console.log(err);
                        $rootScope.dataLoading = false;
                    });

                },
                eventDrop: $scope.eventDrop,
                eventResize: $scope.eventResize
            }
        };

        $scope.openModal = function(existing, size) {
            if (existing === false) { // modal for new

                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'logNewModalContent.html',
                    controller: 'logNewModalCtrl',
                    size: size,
                    scope: $scope
                });
            } else { // modal for editing
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'logExistingModalContent.html',
                    controller: 'logExistingModalCtrl',
                    size: size,
                    scope: $scope
                });
            }

        }
    })

.controller('logNewModalCtrl', function($scope, $rootScope, $window, $uibModalInstance, $log, $http) {

        $scope.openPicker = function(e, picker) {
            $scope.workout[picker].open = true;
        };


        $scope.workout = {};
        $scope.workout.startDate = {
            date: $scope.start.local().toDate()
        };

        if ($scope.end.diff($scope.start, 'hours') >= 24) {
            $scope.workout.endDate = {
                date: $scope.end.subtract(23, 'hours').local().toDate(),
            };
        } else {
            $scope.workout.endDate = {
                date: $scope.end.local().toDate(),
            };
        }

        $scope.workout.startDate.buttonBar = {
            clear: {
                show: false
            }
        };
        //$scope.workout.startDate = $scope.start.local();
        $scope.workout.color = '#3a87ad';
        $scope.currentTime = new Date();
        $scope.hStep = 1;
        $scope.mStep = 1;
        $scope.ismeridian = true;

        $scope.update = function() {
            var d = new Date();
            d.setHours(14);
            d.setMinutes(0);
            $scope.workout.startDate = d;
        };

        $scope.changed = function(t) {
            //$log.log('Time changed to: ' + $scope.workout.startDate);
        };

        $scope.clear = function() {
            $scope.mytime = null;
        };


        $scope.save = function() {
            var accessToken = $window.localStorage.getItem('accessToken');

            var w = {
                name: $scope.workout.name,
                startDate: $scope.workout.startDate.date,
                endDate: $scope.workout.endDate.date
            };

            $rootScope.dataLoading = true;
            $http({
                method: 'POST',
                url: './workout',
                data: {
                    accessToken: accessToken,
                    workout: w
                }
            }).then(function(response) {

                var e = {
                    id: response.data._id,
                    title: response.data.name,
                    start: moment(response.data.startDate),
                    color: response.data.calendarEventColor,
                    stick: true
                };

                $scope.eventSources[0].push(e);
                //$log.log($scope.eventSources);
                $rootScope.dataLoading = false;
            }, function(err) {
                console.log(err);
                $rootScope.dataLoading = false;
            });
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('logExistingModalCtrl', function($scope, $rootScope, $uibModalInstance, $log, $http, $window, AuthenticationService) {
        var accessToken = $window.localStorage.getItem('accessToken');

        $scope.removeWorkoutPopoverIsOpen = false;

        $scope.workout = $scope.selectedWorkout;
        $scope.currentTime = new Date();
        $scope.hStep = 1;
        $scope.mStep = 1;
        $scope.ismeridian = true;

        //$scope.workout.end = moment($scope.workout.start).add(1, 'hour');

        $rootScope.dataLoading = true;
        $http({
            method: 'GET',
            url: './workout/' + $scope.workout.id + '/?accessToken=' + accessToken,
        }).then(function(response) {


            $scope.selectedWorkout.startDate = {
                date: moment(response.data.startDate).local().toDate()
            };
            $scope.selectedWorkout.endDate = {
                date: moment(response.data.endDate).local().toDate()
            };

            $scope.openPicker = function(e, picker) {
                $scope.selectedWorkout[picker].open = true;
            };

            $scope.selectedWorkout.exercises = [];
            for (var i = 0; i < response.data.exercises.length; i++) {
                $scope.selectedWorkout.exercises[i] = {
                    id: response.data.exercises[i]._id,
                    name: response.data.exercises[i].name,
                    note: response.data.exercises[i].note,
                    sets: response.data.exercises[i].sets
                }

                $scope.selectedWorkout.exercises[i].setQuantity = $scope.selectedWorkout.exercises[i].sets.length;

            }

            $rootScope.dataLoading = false;
        }, function(err) {
            console.log(err);
            $rootScope.dataLoading = false;
        });

        $scope.changed = function(t) {
            //$log.log('Time changed to: ' + $scope.workout.startDate);
        };

        $scope.clear = function() {
            $scope.mytime = null;
        };



        $scope.addExercise = function(id) {
            var token = AuthenticationService.GetToken();

            $rootScope.dataLoading = true;
            $http({
                method: 'POST',
                url: './workout/' + id + '/exercise',
                data: {
                    accessToken: token
                }
            }).then(function(response) {


                var exercise = {
                    id: response.data._id,
                    name: response.data.name,
                    note: response.data.note,
                    sets: response.data.sets
                };
                $scope.selectedWorkout.exercises.push(exercise);
                $rootScope.dataLoading = false;
            }, function(err) {
                console.log(err);
                $rootScope.dataLoading = false;
            });
        };

        $scope.addSet = function(id, eId) {
            var token = AuthenticationService.GetToken();

            $rootScope.dataLoading = true;
            $http({
                method: 'POST',
                url: './workout/' + id + '/exercise/' + eId + '/set',
                data: {
                    accessToken: token
                }
            }).then(function(response) {

                var set = {
                    _id: response.data._id,
                    reps: response.data.reps,
                    weight: response.data.weight,
                    note: response.data.note
                };


                for (var i = 0; i < $scope.selectedWorkout.exercises.length; i++) {
                    if ($scope.selectedWorkout.exercises[i].id === response.data.exercise) {
                        $scope.selectedWorkout.exercises[i].sets.push(set);
                        $scope.selectedWorkout.exercises[i].setQuantity = $scope.selectedWorkout.exercises[i].sets.length;
                        break;
                    }
                }


                $scope.selectedWorkout.exercises[i].setListExpanded = true;

                $rootScope.dataLoading = false;

            }, function(err) {
                console.log(err);
                $rootScope.dataLoading = false;
            });
        };



        $scope.save = function(id) {
            var token = AuthenticationService.GetToken();

            var workout = {
                name: $scope.selectedWorkout.title,
                startDate: $scope.selectedWorkout.startDate.date,
                endDate: $scope.selectedWorkout.endDate.date,
                calendarEventColor: $scope.selectedWorkout.color
            };



            var exercises = $scope.selectedWorkout.exercises;
            $rootScope.dataLoading = true;
            $http({
                method: 'PUT',
                url: './workout/' + id,
                data: {
                    accessToken: token,
                    workout: workout,
                    exercises: exercises
                }
            }).then(function(response) {


                for (var i = 0; i < $scope.eventSources[0].length; i++) {
                    if ($scope.eventSources[0][i].id === response.data._id) {
                        $scope.eventSources[0][i] = {
                            id: response.data._id,
                            start: moment(response.data.startDate),
                            end: moment(response.data.endDate),
                            title: response.data.name,
                            color: response.data.calendarEventColor
                        };
                        break;
                    }
                }

                $uibModalInstance.close();
                $rootScope.dataLoading = false;
            }, function(err) {
                console.log(err);
                $rootScope.dataLoading = false;
            });
        };

        $scope.delete = function(id) {
            var token = AuthenticationService.GetToken();

            $rootScope.dataLoading = true;
            $http({
                method: 'DELETE',
                url: './workout/' + id + '/?accessToken=' + token
            }).then(function(response) {


                for (var i = 0; i < $scope.eventSources[0].length; i++) {
                    if ($scope.eventSources[0][i].id === response.data._id) { // remove event from calendar
                        $scope.eventSources[0].splice(i, 1)
                        break;
                    }
                }

                $uibModalInstance.close();
                $rootScope.dataLoading = false;
            }, function(err) {
                $rootScope.dataLoading = false;
            });
        };
        $scope.deleteExercise = function(id, eId) {
            var token = AuthenticationService.GetToken();

            $rootScope.dataLoading = true;
            $http({
                method: 'DELETE',
                url: './workout/' + id + '/exercise/' + eId + '/?accessToken=' + token
            }).then(function(response) {


                for (var i = 0; i < $scope.selectedWorkout.exercises.length; i++) {
                    if ($scope.selectedWorkout.exercises[i].id === response.data._id) { // remove event from calendar
                        $scope.selectedWorkout.exercises.splice(i, 1)
                        break;
                    }
                }
                $rootScope.dataLoading = false;
            }, function(err) {
                console.log(err);
                $rootScope.dataLoading = false;
            });
        };

        $scope.deleteSet = function(id, eId, sId) {
            var token = AuthenticationService.GetToken();


            $rootScope.dataLoading = true;
            $http({
                method: 'DELETE',
                url: './workout/' + id + '/exercise/' + eId + '/set/' + sId + '/?accessToken=' + token
            }).then(function(response) {


                for (var i = 0; i < $scope.selectedWorkout.exercises.length; i++) {
                    if ($scope.selectedWorkout.exercises[i].id === response.data.exercise) { // remove event from calendar
                        for (var i2 = 0; i2 < $scope.selectedWorkout.exercises[i].sets.length; i2++) {
                            if ($scope.selectedWorkout.exercises[i].sets[i2]._id === response.data._id) { // remove event from calendar
                                $scope.selectedWorkout.exercises[i].sets.splice(i2, 1)
                                $scope.selectedWorkout.exercises[i].setQuantity = $scope.selectedWorkout.exercises[i].sets.length;
                                break;
                            }
                        }
                    }

                }


                $rootScope.dataLoading = false;

            }, function(err) {
                console.log(err);
                $rootScope.dataLoading = false;
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    });
