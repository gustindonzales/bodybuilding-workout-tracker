'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('MainCtrl', function($scope, $location, AuthenticationService) {

    $scope.logout = function(){
      AuthenticationService.Logout(function(response){
        if(response.success){
          $location.path('/login');
        }
      });
    };
  });
