module.exports = function(jwt, cryptr, config, moment, failedLoginModel){
  return {
    checkToken: function(req, res, next){

      var token = req.body.accessToken || req.query.accessToken || null;


      if(token == null){
        res.status(401).send("token does not exist.");
        return;
      }

      var token = cryptr.decrypt(token);
      // invalid token - synchronous
      try {
        var token = jwt.verify(token, config.jwt.privateKey);
      } catch(err) {
        console.log('invalid token');
        res.status(401).send('Bad token');
        return;
      }

      req.accessToken = token;

      next();

    },

    checkLockStatus: function(req, res, next){
      var client = {
        remoteAddress: req.connection.remoteAddress
      };
      console.log(req.connection.remoteAddress);

      failedLoginModel.findOne({remoteAddress: client.remoteAddress}, function(err, doc){
        if(err){
          res.status(500).send(err);
          return;
        }

        if(doc){ // a failed login exists
          if(doc.attempts >= 5){ // 5 failed attempts, investigate further
            if(doc.locked === true){ // currently locked out. check if ip can be unlocked.
              if(doc.unlockTime < new Date()){ // ip can be unlocked
                failedLoginModel.update({remoteAddress: doc.remoteAddress}, {$set: {attempts: 0, locked: false}}, {new: true}, function(err){
                  if(err){
                    res.status(500).send(err);
                    return;
                  }

                });
              } else { // ip is still in the lockout period
                var startDate = moment(doc.unlockTime);
                var endDate = moment();
                var secondsDiff = endDate.diff(startDate, 'seconds');
                res.send({success: false, msg: "too many failed login attempts from this IP. You can try again in " + secondsDiff + " seconds"});
                return;
              }
            }
          }
        }

        next();
      });

      return;

      if(typeof access_token === 'undefined' || access_token === ''){
        res.status(401).send('cookie doesn\'t exist');
        return;
      }

      try {
        var decoded = jwt.verify(encryption.decrypt(access_token), config.tokenSecret);
      } catch(err) {
        res.clearCookie('access_token');
        res.status(401).send('naughty naughty, the jwt has been modified!!!: ' + err);
        return;
      }

      User.findById(decoded.userId, function(err,  doc){

        if(err){
          res.status(500).send('Error occured while fetching user data.');
          return;
        }

        if(doc === null){ // cannot find user
          res.status(401).send('cannot find user in database.');
          return;
        }


        if(doc.tokenExpiryDate.getTime() < new Date().getTime()){// token is expired, get a new one
          console.log('token is expired, attempting to get new one.');

          oauth2Client.setCredentials({
            refresh_token: doc.refreshToken
          });
          oauth2Client.refreshAccessToken(function(err, tokens) {
            if(err){
              //res.status(500).send(err);
              console.log(err);
              console.log(doc.refreshToken);
              return;
            }

            User.update({_id: doc._id}, {accessToken: tokens.access_token, tokenExpiryDate: new Date(tokens.expiry_date)}, function(err, d){
              if(err){
                req.status(500).send(err);
                return;
              }
            });
          });
        }

        user_data = decoded;
        next();

      });


    }

  }
}
